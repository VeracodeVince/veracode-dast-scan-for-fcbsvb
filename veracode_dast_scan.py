#!/usr/bin/env python
import sys
import requests
from veracode_api_signing.plugin_requests import RequestsAuthPluginVeracodeHMAC
from argparse import ArgumentParser
import datetime
import time
import logging
import pytimeparse
import urllib.parse

parser = ArgumentParser()
parser.add_argument("-vid", required=True, help="Veracode API ID")
parser.add_argument("-vkey", required=True, help="Veracode API KEY")
parser.add_argument("-scan", required=True, help="DAST Scan Profile Name")
parser.add_argument("-duration", default=1, help="Maxium scan duration in days")
parser.add_argument("-policy", default=False, help="Wait for the scan results and check the linked policy")
parser.add_argument("-timeout", default="45m", help="Timeout in minutes")
parser.add_argument("-fail", default=False, help="Fail job if scan is already running")
args = parser.parse_args()

logging.basicConfig(
    level=logging.INFO,  # Set the desired log level
    format='%(asctime)s [%(levelname)s] - %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

api_base = "https://api.veracode.com/was/configservice/v1"
api_app = "https://api.veracode.com/appsec/v1/applications"
headers = {"User-Agent": "Start DA Scan Example", 'Content-type': 'application/json'}
interval = 60 #Seconds


#Payload for updating schedule of existing DA job to start immediately
data =   { 
    "schedule": 
        {       
            "now": True,
            "duration": 
                {
                "length": args.duration,
                "unit": "DAY"
                }
        }
}

def get_linked_app(analysis_id):
    res = requests.get(api_base + "/analyses/" + analysis_id + "/scans", auth=RequestsAuthPluginVeracodeHMAC(api_key_id=args.vid, api_key_secret=args.vkey), headers=headers)
    response = res.json()
    try:
        # At the moment we will only review the first analysis in the DAST profile.
        linked_app = response['_embedded']['scans'][0]['linked_platform_app_name']
        linked_guid = response['_embedded']['scans'][0]['linked_platform_app_uuid']
        logging.debug("Linked app GUID: %s" % linked_guid)
        logging.info("Found linked application profile : %s" % linked_app)
    except:
        logging.error("Unable to identify linked application profile")
        exit(1)

    res = requests.get(api_app + "/" + linked_guid, auth=RequestsAuthPluginVeracodeHMAC(api_key_id=args.vid, api_key_secret=args.vkey), headers=headers)
    response = res.json()

    policy_date = response['last_policy_compliance_check_date']

    return linked_guid, linked_app, policy_date



def check_policy(app_guid, app_name, old_policy_date):
    duration_seconds = pytimeparse.parse(args.timeout)
    logging.info("Timeout set to : %s (%d seconds)" % (args.timeout,duration_seconds))
    
    # Wait for the analysis to queue up
    while True:
        try:
            res = requests.get(api_base + "/analyses/" + job_id, auth=RequestsAuthPluginVeracodeHMAC(api_key_id=args.vid, api_key_secret=args.vkey), json=data, headers=headers)
            response = res.json()
        except:
            logging.error("Unable to get DAST analysis status")
            sys.exit(1)
        status = response['latest_occurrence_status']['status_type']
        status_date = response['last_modified_on']
        status_date = status_date.replace("[UTC]", "")
        status_date_object = datetime.datetime.strptime(status_date, "%Y-%m-%dT%H:%M:%SZ")
        if status_date_object > current_time or args.fail == False :
            if status in ("STOPPED_TECHNICAL_ISSUE", "VERIFICATION_FAILED"):
                logging.info("Scan status : %s" % status)
                logging.error("Dynamic analysis stopped with technical error")
                sys.exit(1)
            elif status in ("SUBMITTED"):
                logging.info("Scan status : %s" % status)
                logging.info("Dynamic anlaysis has been queued waiting for completion")
            elif status in ("IN_PROGRESS"):
                logging.info("Scan status : %s" % status)
                logging.info("Dynamic analysis is in progress")
            elif status in ("FINISHED_RESULTS_AVAILABLE", "FINISHED_RESULTS_AVAILABLE_WITH_WARNING"):
                logging.info("Scan status : %s" % status)   
                logging.info("Dynamic analysis has finished")
                break
            else:
                logging.info("Dynamic analysis status: %s" % status)
        time.sleep(30)

    while True:
        res = requests.get(api_app + "/" + app_guid, auth=RequestsAuthPluginVeracodeHMAC(api_key_id=args.vid, api_key_secret=args.vkey), headers=headers)
        response = res.json()
    
        try:
            for i in response['scans']:
                if i['scan_type'] == "DYNAMIC":
                    dynamic_status = i['status']
                    logging.info("Current scan status: %s" % dynamic_status)
    
                    if dynamic_status == "PUBLISHED":
                        logging.info("Dynamic analysis complete. Waiting for policy evaluation..")
                        #Wait for the policy evaluation to complete to avoid race condition
                        #Dynamic analysis complete - Check policy evaluation
                        policy_date = response['last_policy_compliance_check_date']
                        date_object = datetime.datetime.strptime(policy_date, "%Y-%m-%dT%H:%M:%S.%fZ")
                        policy_status = response['profile']['policies'][0]['policy_compliance_status']
                        logging.info('Last policy evaluation date : %s' % date_object)
    
                        if date_object > current_time and policy_date != old_policy_date:
                            # Policy evaluation has completed 
                            if policy_status == "PASSED":
                                # Policy has passed we
                                logging.info("Policy Passed")
                                return True
                            elif policy_status == "DID_NOT_PASS":
                                logging.warning("Policy Evaluation completed at %s %s:" %(date_object, policy_status))
                                return False
                            else:
                                logging.info("Policy status : %s" % policy_status)
                                continue
                        else:
                            logging.info("Waiting for new policy evaluation to complete...")
                            time_difference = datetime.datetime.utcnow() - current_time
                            if time_difference.total_seconds() >= duration_seconds:
                                logging.warning("Timeout of %s has expired. Exiting.." % args.timer)
                                sys.exit(1)
                time.sleep(interval)

    
        except Exception as e:
            print("Unable to find dynamic analysis status for application %s" % app_name)
            print(str(e))
            exit(1)
    
def submit_scan():
    logging.info("Looking for Dynamic Analysis Job: " + args.scan)
    scan_name = urllib.parse.quote(args.scan)
    res = requests.get(api_base + "/analyses", auth=RequestsAuthPluginVeracodeHMAC(api_key_id=args.vid, api_key_secret=args.vkey), params={ "name": scan_name }, headers=headers)
    response = res.json()
    try:
        scan_name = response['_embedded']['analyses'][0]['name']
        if scan_name == args.scan:
            job_id = response['_embedded']['analyses'][0]['analysis_id']
            logging.info("found job_id: " + job_id)
        else:
            logging.error("Incorrect Scan ID - scan : %s" % scan_name)
            sys.exit(1)
    except: 
        logging.error("Could not find Dynamic Analysis %s" % args.scan)
        sys.exit(1)
    
    try:
        ### Add logic to check the current status of the scan - is a scan already running ?

        res = requests.put(api_base + "/analyses/" + job_id + '?method=PATCH', auth=RequestsAuthPluginVeracodeHMAC(api_key_id=args.vid, api_key_secret=args.vkey), json=data, headers=headers)
        
        if res.status_code == 204:
            logging.info("New Scan Submitted Successfully: " + str(res.status_code) )
        else:
            response = res.json()
            logging.warning("Unable to submit new Dynamic analysis for scan : %s" % args.scan)
            logging.warning(response['_embedded']['errors'][0]['title'])
            if args.fail:
                logging.error(response['_embedded']['errors'][0]['detail'])
                sys.exit(1)

    except:
        logging.error("Error executing API Call")
        sys.exit(1)
    return job_id
    

if __name__ == "__main__":
    current_time = datetime.datetime.utcnow()#

    job_id = submit_scan()#

    if args.policy:
        linked_guid, linked_app, policy_date = get_linked_app(job_id)
        logging.info("Waiting for scan to complete")
        time.sleep(30)
        # Wait for status to update
        logging.info("Checking for policy evaluation...")
        compliance = check_policy(linked_guid, linked_app, policy_date)
        if compliance == True:
            logging.info("Policy evaluation Successful")
            sys.exit(0)
        else:
            logging.info("Policy evaluation Failed")
            sys.exit(1)
    
